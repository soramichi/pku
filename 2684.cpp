#include <iostream>
#include <vector>

using namespace std;

class point{
public:
  int x, y;
  point(int _x, int _y): x(_x), y(_y){ ; }
};

int operator -(const point& p1, const point& p2){
  if(p1.x == p2.x)
    return (p1.y > p2.y ? p1.y - p2.y : p2.y - p1.y);
  else if(p1.y == p2.y)
    return (p1.x > p2.x ? p1.x - p2.x : p2.x - p1.x);
  else
    cerr << "Error in calculating a distance of points. input miss-read??" << endl;
}

class angle{
public:
  int direction; // 0: right, 1: left;
  angle(const point& start, const point& intermid, const point& end){
    if(start.x == intermid.x){
      if(intermid.y > start.y){
	if(end.x > intermid.x)
	  direction = 0;
	else
	  direction = 1;
      }
      else if(intermid.y < start.y){
	if(end.x > intermid.x)
	  direction = 1;
	else
	  direction = 0;
      }
    }
    else if(start.y == intermid.y){
      if(intermid.x > start.x){
	if(end.y > intermid.y)
	  direction = 1;
	else
	  direction = 0;
      }
      else if(intermid.x < start.x){
	if(end.y > intermid.y)
	  direction = 0;
	else
	  direction = 1;
      }
    }
  }
};

bool operator ==(const angle& a1, const angle& a2){
  return a1.direction == a2.direction;
}
  
class line{
public:
  vector<point> points;
  vector<int> distances;
  vector<angle> angles;
  vector<angle> angles_reverse;

  void add(const point& p){
    points.push_back(p);

    int l = points.size();

    if(l >= 2)
      distances.push_back(points[l-1] - points[l-2]);

    if(l >= 3){
      angles.push_back(angle(points[l-3], points[l-2], points[l-1]));
      angles_reverse.push_back(angle(points[l-1], points[l-2], points[l-3]));
    }
  }
};

int main(){
  int n;

  while(cin >> n, n){
    int m, x, y;
    line original;
    vector<line> suspects;

    cin >> m;

    while(m--){
      cin >> x >> y;
      original.add(point(x, y));
    }

    while(n--){
      line tmp;

      cin >> m;

      while(m--){
	cin >> x >> y;
	tmp.add(point(x, y));
      }

      suspects.push_back(tmp);
    }

    for(int i=0;i<suspects.size();i++){
      if(equal(original.distances.begin(), original.distances.end(), suspects[i].distances.begin()) &&
	 equal(original.angles.begin(), original.angles.end(), suspects[i].angles.begin()))
	cout << i+1 << endl;
      else if(equal(original.distances.begin(), original.distances.end(), suspects[i].distances.rbegin()) &&
	      equal(original.angles.begin(), original.angles.end(), suspects[i].angles_reverse.rbegin()))
	cout << i+1 << endl;
    }

    cout << "+++++" << endl;
  }

  return 0;
}
