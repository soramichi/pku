#include <stdio.h>
#include <stdlib.h>

int calc(int start, int years, int kind, double rate, int fee){
  int i;
  int money = start;
  int reward = 0;

  for(i=0;i<years;i++){
    if(kind == 1){
      reward = (int)(money * rate);
      money = money + reward - fee;
      reward = 0;
    }
    else if(kind == 0){
      reward += (int)(money * rate);
      money = money - fee;
    }
  }

  return money + reward;
}

main(){
  int m;
  scanf("%d\n", &m);

  while(m--){
    int start, years, n;
    int* last;
    int i;

    scanf("%d\n", &start);
    scanf("%d\n", &years);
    scanf("%d\n", &n);

    last = malloc(sizeof(int) * n);

    //    printf("%d %d %d\n", start, years, n);

    for(i=0;i<n;i++){
      int kind, fee;
      double rate;

      scanf("%d %lf %d\n", &kind, &rate, &fee);
      last[i] = calc(start, years, kind, rate, fee);
    }

    int max = 0;
    for(i=0;i<n;i++){
      if(last[i] >= max)
	max = last[i];
    }

    printf("%d\n", max);
  }

  return 0;
}
