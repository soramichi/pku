#include <iostream>
#include <vector>
#include <cstdlib>
#include <algorithm>

using namespace std;

// Sieve of Eratosthenes
void calc_prime(bool is_prime[]){
  //  for(int i=2;i<=99999;i++){
  for(int i=2;i<=400;i++){
    int start = 0;

    for(int j=i;j<=99999;j++){
      if(is_prime[j]){
	start = j;
	break;
      }
    }

    if(start == 0)
      break;

    for(int j=start*start;j<=99999;j+=start){
      is_prime[j] = false;
    }
  }
}

class my_cmp{
private:
  string target;
  int n;
public:
  //  my_cmp(const string& _target, int _n): target(_target), n(_n){ ; }
  my_cmp(const string& _target, int _n){
    target = _target;
    n = _n;    
  }
  
  bool operator()(const string& s){
    if(n>0){
      for(int i=0;i<n;i++)
	if(s[i] != target[i])
	  return false;
    }
    else if(n<0){
      n = -n; // n<0 -> n>0
      for(int i=0;i<n;i++)
	if(s[i] != target[target.size() - i])
	  return false;
    }
    
    return true;
  }
};


class  my_cmp2{
private:
  int n;
public:
  my_cmp2(int _n){
    n = _n;    
  }
  
  bool operator()(const string& s, const string& target){
    if(n>0){
      for(int i=0;i<n;i++)
	if(s[i] != target[i])
	  return s[i] < target[i];
    }
    else if(n<0){
      n = -n; // n<0 -> n>0
      for(int i=0;i<n;i++)
	if(s[i] != target[target.size() - i])
	  return s[i] < target[i];
    }
    
    return false;
  }
};

bool is_even(int n){
  return (n%2 == 0);
}

int main(){
  int sum, start;
  bool is_prime[100000];
  vector<string> primes;

  for(int i=0;i<100000;i++)
    is_prime[i] = true;
  is_prime[0] = is_prime[1] = false;
  calc_prime(is_prime);

  cin >> sum >> start;

  // abcde meets the below conditions:
  //        a+b+c+d+e = sum
  //        the value abcde is a prime number
  for(int a=1;a<=sum;a++)
    for(int b=0;b<=sum-a;b++)
      for(int c=0;c<=sum-a-b;c++)
	for(int d=0;d<=sum-a-b-c;d++){
	  string s;
	  int e = sum-a-b-c-d;

	  if(e >= 10)
	    continue;

	  s += a+'0'; s += b+'0'; s += c+'0'; s += d+'0'; s += e+'0';

	  if(is_prime[atoi(s.c_str())])
	    primes.push_back(s);
	}

  //  for(int i=0;i<primes.size();i++)
  //      cout << primes[i] << endl;

  for(int i=0;i<primes.size();i++){
    // prime[i] is a candidate for the frist column
    if(primes[i][0] == start + '0' && find(primes[i].begin(), primes[i].end(), '0') == primes[i].end()){
      vector<string> row[5];
      bool found = true;
      
      //      cout << primes[i] << endl;

      // find 5 primes that can be candidates of the answer,
      // which means the 1st characeter of a candidate is equal to the characeter of the right position in the 1st column
      for(int r=0;r<5;r++){
	for(int j=0;j<primes.size();j++){
	  string candidate = primes[j];

	  if(candidate[0] == primes[i][r] && (r==0 ? find(candidate.begin(), candidate.end(), '0') == candidate.end() : true) && (r==4 ? find_if(candidate.begin(), candidate.end(), is_even) == candidate.end() : true)){
	    row[r].push_back(candidate);
	  }
	}
	
	if(row[r].size() == 0)
	  found = false;
      }

      if(!found)
	continue;


      //      cout << "1st column: " << primes[i] << endl;
      //      cout << row[0].size() << ", "<< row[1].size() << ", "<< row[2].size() << ", "<< row[3].size() << ", "<< row[4].size() << endl;

      /*
      for(int r=0;r<5;r++)
	cout << row[r] << endl;
      cout << "-----" << endl;
      */

      
      for(int r0=0;r0<row[0].size();r0++){
	string t2;

	if(atoi(row[0][r0].c_str()) < atoi(primes[i].c_str()))
	  goto r0_end;

	t2 += row[0][r0][4];
	//	if(find_if(primes.begin(), primes.end(), my_cmp(t2, -1)) == primes.end())
	if(!binary_search(primes.begin(), primes.end(), t2, my_cmp2(-1)))
	  goto r0_end;

	for(int l=0;l<5;l++){
	  string s;
	  s += row[0][r0][l];
	  
	  //	  if(find_if(primes.begin(), primes.end(), my_cmp(s, 1)) == primes.end())
	if(!binary_search(primes.begin(), primes.end(), s, my_cmp2(1)))
	    goto r0_end;
	}

	for(int r1=0;r1<row[1].size();r1++){
	  string t1, t2;
	  t1 += row[0][r0][0];
	  t1 += row[1][r1][1];
	  t2 += row[0][r0][4];
	  t2 += row[1][r1][3];
	  //	  if(find_if(primes.begin(), primes.end(), my_cmp(t1, 2)) == primes.end() || find_if(primes.begin(), primes.end(), my_cmp(t2, -2)) == primes.end())
	  if(!binary_search(primes.begin(), primes.end(), t1,  my_cmp2(2)) || !binary_search(primes.begin(), primes.end(), t2, my_cmp2(-2)))
	    goto r1_end;

	  for(int l=1;l<5;l++){
	    string s;
	    s += row[0][r0][l];
	    s += row[1][r1][l];

	    //	    if(find_if(primes.begin(), primes.end(), my_cmp(s, 2)) == primes.end())
	    if(!binary_search(primes.begin(), primes.end(), s, my_cmp2(2)))
	      goto r1_end;
	  }

	  for(int r2=0;r2<row[2].size();r2++){
	    string t1, t2;
	    t1 += row[0][r0][0];
	    t1 += row[1][r1][1];
	    t1 += row[2][r2][2];
	    t2 += row[0][r0][4];
	    t2 += row[1][r1][3];
	    t2 += row[2][r2][2];

	    //	    if(find_if(primes.begin(), primes.end(), my_cmp(t1, 3)) == primes.end() || find_if(primes.begin(), primes.end(), my_cmp(t2, -3)) == primes.end())
	    if(!binary_search(primes.begin(), primes.end(), t1, my_cmp2(3)) || !binary_search(primes.begin(), primes.end(), t2, my_cmp2(-3)))
	      goto r2_end;

	    for(int l=1;l<5;l++){
	      string s;
	      s += row[0][r0][l];
	      s += row[1][r1][l];
	      s += row[2][r2][l];

	      //	      if(find_if(primes.begin(), primes.end(), my_cmp(s, 3)) == primes.end())
	    if(!binary_search(primes.begin(), primes.end(), s, my_cmp2(3)))
		goto r2_end;
	    }
	    
	    for(int r3=0;r3<row[3].size();r3++){
	      string t1, t2;
	      t1 += row[0][r0][0];
	      t1 += row[1][r1][1];
	      t1 += row[2][r2][2];
	      t1 += row[3][r3][3];
	      t2 += row[0][r0][4];
	      t2 += row[1][r1][3];
	      t2 += row[2][r2][2];
	      t2 += row[3][r3][1];
	      
	      //	      if(find_if(primes.begin(), primes.end(), my_cmp(t1, 4)) == primes.end() || find_if(primes.begin(), primes.end(), my_cmp(t2, -4)) == primes.end())
	      if(!binary_search(primes.begin(), primes.end(), t1, my_cmp2(4)) || !binary_search(primes.begin(), primes.end(), t2, my_cmp2(-4)))
		goto r3_end;
	      
	      for(int l=1;l<5;l++){
		string s;
		s += row[0][r0][l];
		s += row[1][r1][l];
		s += row[2][r2][l];
		s += row[3][r3][l];
		
		//		if(find_if(primes.begin(), primes.end(), my_cmp(s, 4)) == primes.end()){
		if(!binary_search(primes.begin(), primes.end(), s, my_cmp2(4)))
		  goto r3_end;
	      }

	      for(int r4=0;r4<row[4].size();r4++){
		string candidates[5];

		// diagonal number that goes bottom to top (d1), top to bottom (d2)
		string d1, d2, d3;

		candidates[0] = row[0][r0];
		candidates[1] = row[1][r1];
		candidates[2] = row[2][r2];
		candidates[3] = row[3][r3];
		candidates[4] = row[4][r4];
		
		// check if all the columns are really primes
		for(int l=1;l<5;l++){
		  string p;
		  
		  for(int r=0;r<5;r++)
		    p += candidates[r][l];
		  
		  if(find(primes.begin(), primes.end(), p) == primes.end())
		    goto r4_end;
		}
		
		d1 += candidates[4][0]; d1 += candidates[3][1]; d1 += candidates[2][2]; d1 += candidates[1][3]; d1 += candidates[0][4]; 
		d2 += candidates[0][0]; d2 += candidates[1][1]; d2 += candidates[2][2]; d2 += candidates[3][3]; d2 += candidates[4][4]; 
		d3 += candidates[0][4]; d3 += candidates[1][3]; d3 += candidates[2][2]; d3 += candidates[3][1]; d3 += candidates[4][0];

		// check if the two diagonal numbers are really primes
		//if(find(primes.begin(), primes.end(), d1) != primes.end() && find(primes.begin(), primes.end(), d2) != primes.end()){
		if(binary_search(primes.begin(), primes.end(), d1)  && binary_search(primes.begin(), primes.end(), d2)){
		  //		  cout << "d1: " << d1 << endl;
		  //		  cout << "d3: " << d3 << endl;
		  for(int r=0;r<5;r++)
		    cout << candidates[r] << endl;
		  // blank line after each answer
		  cout << endl;
		}
		
		//if(find(primes.begin(), primes.end(), d3) != primes.end() && find(primes.begin(), primes.end(), d2) != primes.end()){
		if(binary_search(primes.begin(), primes.end(), d3)  && binary_search(primes.begin(), primes.end(), d2)){
		  string swapped[5];
		  
		  for(int i=0;i<5;i++)
		    for(int j=0;j<5;j++)
		      swapped[i] += candidates[j][i];
		  
		  bool same = true;
		  for(int r=0;r<5;r++)
		    if(swapped[r] != candidates[r])
		      same = false;

		  if(!same){
		    for(int r=0;r<5;r++)
		      cout << swapped[r] << endl;
		    // blank line after each answer
		    cout << endl;
		  }
		}
	       
	      r4_end:;
	      }
	    r3_end:;
	    }
	  r2_end:;
	  }
	r1_end:;
	}
      r0_end:;
      }
    }
  }

  return 0;
}
