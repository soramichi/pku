#include <stdio.h>

int calc(int dp[][100], int W, int H, int S, int T){
  int i, j, max=0;

  for(i=0;i<W;i++){
    for(j=0;j<H;j++){
      if(i==0 && j==0){ ; }
      else if(i==0)
	dp[i][j] += dp[i][j-1];
      else if(j==0)
	dp[i][j] += dp[i-1][j];
      else
	dp[i][j] += (dp[i-1][j] + dp[i][j-1] - dp[i-1][j-1]);
    }
  }

  for(i=0;i<=W-S;i++){
    for(j=0;j<=H-T;j++){
      int trees;
      if(i==0 && j==0)
	trees = dp[i+S-1][j+T-1];
      else if(i==0)
	trees = dp[i+S-1][j+T-1] - dp[i+S-1][j-1];
      else if(j==0)
	trees = dp[i+S-1][j+T-1] - dp[i-1][j+T-1];
      else
	trees = dp[i+S-1][j+T-1] - dp[i+S-1][j-1] - dp[i-1][j+T-1] + dp[i-1][j-1];

      max = (trees>max ? trees : max);
    }
  }

  return max;
}

main(){
  int N;

  while(scanf("%d\n", &N), N){
    int field[100][100] = {};
    int W, H, S, T;

    scanf("%d %d\n", &W, &H);

    while(N--){
      int x, y;

      scanf("%d %d\n", &x, &y);
      field[x-1][y-1] = 1;
    }

    scanf("%d %d\n", &S, &T);

    printf("%d\n", calc(field, W, H, S, T));
  }

  return 0;
}
