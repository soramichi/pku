#include <stdio.h>
#include <string.h>

void search(char stage[][20], int W, int H, int start_W, int start_H){
  if(start_H < 0 || start_W < 0 || start_H >= H || start_W >= W)
    return;
  else if(stage[start_H][start_W] == '#' || stage[start_H][start_W] == '*')
    return;

  stage[start_H][start_W] = '*';

  search(stage, W, H, start_W + 1, start_H);
  search(stage, W, H, start_W - 1, start_H);
  search(stage, W, H, start_W, start_H + 1);
  search(stage, W, H, start_W, start_H - 1);
}

main(){
  int W, H, i, j;
  int start_W, start_H;
  char stage[20][20];

  while(1){
    int ans = 0;

    scanf("%d %d\n", &W, &H);

    if(!W && !H)
      break;

    for(i=0;i<H;i++){
      for(j=0;j<W;j++){
	stage[i][j] = getchar();

	if(stage[i][j] == '@'){
	  start_H = i;
	  start_W = j;
	}
      }

      // skip '\n'
      getchar();
    }

    search(stage, W, H, start_W, start_H);

    for(i=0;i<H;i++)
      for(j=0;j<W;j++)
	if(stage[i][j] == '*')
	  ans++;

    printf("%d\n", ans);
  }

  return 0;
}
