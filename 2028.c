#include <stdio.h>
#include <string.h>

main(){
  int N, Q, i;
  
  while(1){
    int dates[100] = {};
    int ans = 0;

    scanf("%d %d\n", &N, &Q);
    
    if(N==0 && Q==0)
      break;

    while(N--){
      int M;
      scanf("%d", &M);

      while(M--){
	int tmp;
	scanf("%d", &tmp);
	dates[tmp]++;
      }
    }

    for(i=0;i<100;i++){
      if(dates[i] >= Q && dates[i] > dates[ans]){
	ans = i;
      }
    }

    printf("%d\n", ans);
  }

  return 0;
}
