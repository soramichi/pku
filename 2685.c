#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int calc(char* str){
  int v, i;
  int prefix = 1;
  
  for(v=i=0;i<strlen(str);i++){
    if(str[i] >= '2' && str[i] <= '9')
      prefix = str[i] - '0';
    else{
      if(str[i] == 'm')
    	v += prefix * 1000;
      else if(str[i] == 'c')
    	v += prefix * 100;
      else if(str[i] == 'x')
    	v += prefix * 10;
      else if(str[i] == 'i')
    	v += prefix;
      else
	printf("Error!\n");
      
      prefix = 1;
    }
  }
  
  return v;
}

char* revert(int v){
  char* ret = (char*)malloc(sizeof(char) * 32);
  int index = 0;
  
  if(v >= 1000){
    if(v >= 2000)
      ret[index++] = v / 1000 + '0';
    
    ret[index++] = 'm';
    v %= 1000;
  }
  if(v >= 100){
    if(v >= 200)
      ret[index++] = v / 100 + '0';
    
    ret[index++] = 'c';
    v %= 100;
  }
  if(v >= 10){
    if(v >= 20)
      ret[index++] = v / 10 + '0';
    
    ret[index++] = 'x';
    v %= 10;
  }
  if(v >= 1){
    if(v >= 2)
      ret[index++] = v / 1 + '0';
    
    ret[index++] = 'i';
  }
  
  ret[index] = '\0';
  
  return ret;
}

main(){
  int n;
  char s1[32], s2[32];
  int v1, v2, i;
  int prefix;
  
  scanf("%d\n", &n);
  
  while(n--){
    int sum;
    
    scanf("%s %s\n", s1, s2);
    v1 = calc(s1);
    v2 = calc(s2);
    sum = v1 + v2;
    
    char* ans = revert(sum);
    printf("%s\n", ans);
    free(ans);
  }
  
  return 0;
}

