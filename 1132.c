#include <stdio.h>
#include <string.h>

main(){
  int n, i, j;
  
  scanf("%d\n", &n);

  for(i=0;i<n;i++){
    int x, y;
    char bitmap[32][32] = {};
    char str[32*32+1];

    scanf("%d %d\n", &x, &y);
    scanf("%s\n", str);

    for(j=0;j<strlen(str);j++){
      if(str[j] == '.')
	break;
      else if(str[j] == 'E')
	bitmap[x++][y-1] = 1;
      else if(str[j] == 'W')
	bitmap[--x][y] = 1;
      else if(str[j] == 'N')
	bitmap[x][y++] = 1;
      else if(str[j] == 'S')
	bitmap[x-1][--y] = 1;
    }
    
    printf("Bitmap #%d\n", i+1);

    for(y=31;y>=0;y--){
      for(x=0;x<32;x++){
	printf("%c", bitmap[x][y] ? 'X' : '.');
      }
      printf("\n");
    }

    printf("\n");
  }

  return 0;
}
