#include <iostream>
#include <algorithm>
#include <cstring>

using namespace std;

int main(){
  while(1){
    int k1, k2 ,k3;
    string encrypted, g1, g2, g3, decrypted;
    int g1_pos[80], g2_pos[80], g3_pos[80];
    int g1_ptr = 0, g2_ptr = 0, g3_ptr = 0;

    cin >> k1 >> k2 >> k3;

    memset(g1_pos, -1, 80);
    memset(g2_pos, -1, 80);
    memset(g3_pos, -1, 80);

    if(!k1 && !k2 && !k3)
      break;

    cin >> encrypted;

    for(int i=0;i<encrypted.size();i++){
      char c = encrypted[i];

      if(c >= 'a' && c <= 'i'){
	g1 += c;
	g1_pos[g1_ptr++] = i;
      }
      else if(c >= 'j' && c <= 'r'){
	g2 += c;
	g2_pos[g2_ptr++] = i;
      }
      else{
	g3 += c;
	g3_pos[g3_ptr++] = i;
      }
    }
    
    rotate(g1.rbegin(), g1.rbegin() + k1%(g1.size() ? : 1), g1.rend());
    rotate(g2.rbegin(), g2.rbegin() + k2%(g2.size() ? : 1), g2.rend());
    rotate(g3.rbegin(), g3.rbegin() + k3%(g3.size() ? : 1), g3.rend());

    g1_ptr = g2_ptr = g3_ptr = 0;
    for(int i=0;i<encrypted.size();i++){
      if(g1_pos[g1_ptr] == i)
	decrypted += g1[g1_ptr++];
      else if(g2_pos[g2_ptr] == i)
	decrypted += g2[g2_ptr++];
      else if(g3_pos[g3_ptr] == i)
	decrypted += g3[g3_ptr++];
    }

    cout << decrypted << endl;
  }

  return 0;
}
