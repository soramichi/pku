#include <stdio.h>
#include <stdlib.h>
#include <math.h>

main(){
  int n, m, i, j, N = 0;

  while(scanf("%d\n", &n), n){
    int* order = (int*)malloc(sizeof(int) * n);
    char* assign = (char*)malloc(sizeof(char) * ((int)pow(2,n) + 1));
    char* VVA;

    printf("S-Tree #%d:\n", ++N);

    for(i=0;i<n;i++)
      scanf("x%d ", order + i);
    
    gets(assign);

    scanf("%d\n", &m);

    for(i=0;i<m;i++){
      int ptr = (int)pow(2, n-1);

      VVA = (char*)malloc(sizeof(char) * (n + 1));
      gets(VVA);

      for(j=0;j<n;j++){
	if(j<n-1){
	  int diff = (VVA[order[j] - 1] == '1' ? 1 : -1) * (int)pow(2, n-j-2);
	  ptr += diff;
	}
	else if(j==n-1){
	  ptr += (VVA[order[j] - 1] == '1' ? 0 : -1);
	}
      }

      putchar(assign[ptr]);
    }

    printf("\n\n");

    free(order);
    free(assign);
    free(VVA);
  }

  return 0;
}
