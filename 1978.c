#include <stdio.h>
#include <stdlib.h>

void shuffle(int* cards, int p, int c){
  int* buffer = (int*)malloc(sizeof(int) * c);
  int i;

  for(i=0;i<c;i++){
    buffer[i] = cards[i+p-1];
  }

  for(i=p-2;i>=0;i--){
    cards[i+c] = cards[i];
  }
  
  for(i=0;i<c;i++){
    cards[i] = buffer[i];
  }

  free(buffer);
}

main(){
  int n, r, i;
  int p, c;
  int* cards;

  while(1){
    scanf("%d %d\n", &n, &r);
    //    printf(":::%d %d\n", n, r);

    if(n==0 && r==0)
      break;

    cards = malloc(sizeof(int) * n);

    for(i=n;i>=1;i--)
      cards[n-i] = i;

    while(r--){
      scanf("%d %d\n", &p, &c);
      //      printf(":%d %d\n", p, c);

      /*
      for(i=0;i<n;i++){
	printf(":%d\n",cards[i]);
      }
      printf("--\n");
      */
      shuffle(cards, p, c);

    }

    printf("%d\n", cards[0]);
  }

  return 0;
}
